﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTraversalLibrary
{
    internal class Node<T>
    {
        public T Data { get; }
        public List<T> Path { get; }
        
        public Node(T data, List<T> path)
        {
            Data = data;
            Path = path;
        }
    }
}
