﻿using DataTraversalLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TraversalLibrary
{
    public class Traverser<T>
    {
        IHierarchy<T> Source;
        public Traverser(IHierarchy<T> source)
        {
            Source = source;
        }

        Queue<Node<T>> nodesQueue = new Queue<Node<T>>();


        public delegate void TraversalDelegate(T element, List<T> path, int level);

        public void Traverse(TraversalDelegate callback, CancellationToken cancellationToken)
        {
            foreach (var root in Source.GetRoots())
            {
                List<T> path = new List<T>() { root };
                nodesQueue.Enqueue(new Node<T>(root, path));
            }

            if (nodesQueue.Count > 0)
            {
                TraverseInternal(nodesQueue.Peek().Data, callback, cancellationToken);
            }
            else throw new NullReferenceException("The Sourse provided is empty");
        }

        void TraverseInternal(T root, TraversalDelegate callback, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
            }

            var rootNode = nodesQueue.Dequeue();
            callback(rootNode.Data, rootNode.Path, rootNode.Path.Count);

            foreach (var child in Source.GetChildren(root))
            {
                List<T> path = new List<T>();
                path.AddRange(rootNode.Path);
                path.Add(child);
                nodesQueue.Enqueue(new Node<T>(child, path));
                if (cancellationToken.IsCancellationRequested)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                }
            }
            if (nodesQueue.Count > 0)
            {
                TraverseInternal(nodesQueue.Peek().Data, callback, cancellationToken);
            }
        }
    }

}

