﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraversalLibrary
{
    public interface IHierarchy<T>
    {
        List<T> GetRoots();

        List<T> GetChildren(T parent);
    }
}
