﻿using System;
using System.Collections.Generic;
using System.Text;
using TraversalLibrary;

namespace CATraverse
{
    class DBHierarchy : IHierarchy<Category>
    {
        SearchEngine se = new SearchEngine();

        public List<Category> GetRoots()
        {
            return se.GetCategoriesWithParentId(null);
        }

        public List<Category> GetChildren(Category parent)
        {
            return se.GetCategoriesWithParentId(parent.Id);
        }
    }
}
