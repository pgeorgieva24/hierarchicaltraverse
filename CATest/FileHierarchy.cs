﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TraversalLibrary;

namespace CATraverse
{
    class FileHierarchy : IHierarchy<FileInfo>
    {
        public List<FileInfo> GetChildren(FileInfo parent)
        {
            List<FileInfo> files = new List<FileInfo>();
            var list = new List<string>();
            if (Directory.Exists(@parent.FullName))
            {
                list.AddRange(Directory.EnumerateFiles(@parent.FullName));
                list.AddRange(Directory.EnumerateDirectories(@parent.FullName));
            }
            foreach (string item2 in list)
            {
                FileInfo f = new FileInfo(item2);

                files.Add(f);
            }
            return files;            
        }

        public List<FileInfo> GetRoots()

        {
            List<FileInfo> roots = new List<FileInfo>();

            if (Directory.Exists(@"C:\Users\User\Desktop\folder"))
            {
                roots.Add(new FileInfo(@"C:\Users\User\Desktop\folder"));
            }

            return roots;
        }
    }
}
