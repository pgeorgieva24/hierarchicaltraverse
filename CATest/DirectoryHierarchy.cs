﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TraversalLibrary;

namespace CATraverse
{
    public class DirectoryHierarchy : IHierarchy<DirectoryInfo>
    {
        public List<DirectoryInfo> GetChildren(DirectoryInfo parent)
        {
            List<DirectoryInfo> directories = new List<DirectoryInfo>();
            var list = Directory.EnumerateDirectories(@parent.FullName);
            foreach (string item2 in list)
            {
                DirectoryInfo f = new DirectoryInfo(item2);

                directories.Add(f);

            }
            return directories;
        }

        public List<DirectoryInfo> GetRoots()
        {
            List<DirectoryInfo> roots = new List<DirectoryInfo>();

            if (Directory.Exists(@"C:\Users\User\Desktop\"))
            {
                roots.Add(new DirectoryInfo(@"C:\Users\User\Desktop\"));
            }

            return roots;
        }
    }
}
