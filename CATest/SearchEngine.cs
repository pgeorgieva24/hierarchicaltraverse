﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace CATraverse
{
    public class SearchEngine
    {
        readonly string connectionString;
        public SearchEngine()
        {
            connectionString = @"Server=BITWC\SQLEXPRESS;Database=HierarchicalDataTraverseDB;Trusted_Connection=True;";
        }

        public List<Category> GetCategoriesWithParentId(int? parentId)
        {
            List<Category> categories = new List<Category>();
            Category category = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = @"select Id, Name, ParentId
                        from Category
                        where ParentId";
                if (parentId == null)
                {
                    query += @" is null ";
                }
                else
                {
                    query += @" =@ParentId ";
                }

                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    if (parentId != null)
                    {
                        SqlParameter sqlParameter = command.Parameters.AddWithValue("@ParentId", parentId);
                    }

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (parentId == null)
                            {
                                category = new Category
                                {
                                    Id = (int)reader["Id"],
                                    Name = reader["Name"].ToString(),
                                };
                            }
                            else
                            {
                                category = new Category
                                {
                                    Id = (int)reader["Id"],
                                    Name = reader["Name"].ToString(),
                                    ParentId = (int)reader["ParentId"]
                                };
                            }
                            categories.Add(category);
                        }

                    }
                }
            }
            return categories;
        }
    }
}
