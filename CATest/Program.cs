﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TraversalLibrary;


namespace CATraverse
{
    class Program
    {
        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            var ct = cts.Token;

            //DirectoryHierarchy directorySourse = new DirectoryHierarchy();
            //Traverser<DirectoryInfo> directoriesTr = new Traverser<DirectoryInfo>(directorySourse);
            //var directoryTraverseTask = Task.Run(() => directoriesTr.Traverse(PrintDirectories, ct), ct);
            //char ch = Console.ReadKey().KeyChar;
            //if (ch == 'c' || ch == 'C')
            //{
            //    cts.Cancel();
            //}

            //DBHierarchy dbSource = new DBHierarchy();
            //Traverser<Category> tr = new Traverser<Category>(dbSource);
            //var dbTraverseTask = Task.Run(() => tr.Traverse(Print, ct), ct);
            //char c = Console.ReadKey().KeyChar;
            //if (c == 'c' || c == 'C')
            //{
            //    cts.Cancel();
            //}

            FileHierarchy fileSource = new FileHierarchy();
            Traverser<FileInfo> fileTr = new Traverser<FileInfo>(fileSource);
            var fileTraverseTask = Task.Run(() => fileTr.Traverse(PrintFiles, ct), ct);
            char c = Console.ReadKey().KeyChar;
            if (c == 'c' || c == 'C')
            {
                cts.Cancel();
            }

            try
            {
                //directoryTraverseTask.Wait();
                //dbTraverseTask.Wait();
                fileTraverseTask.Wait();
            }
            catch (AggregateException ae)
            {
                if (ae.InnerExceptions.Any(e => e is TaskCanceledException))
                    Console.WriteLine("\nTask was cancelled");
                else
                    Console.WriteLine(ae.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                cts.Dispose();
            }

            Console.ReadLine();
        }

        public static void Print(Category element, List<Category> pathItems, int level)
        {
            string path = "";
            foreach (var item in pathItems)
            {
                path += "/" + item.Name;
            }
            Console.WriteLine(element.Name + ", " + path + ", " + level);
        }

        public static void PrintDirectories(DirectoryInfo element, List<DirectoryInfo> pathItems, int level)
        {
            string path = "";
            foreach (var item in pathItems)
            {
                path += "/" + item.Name;
            }
            Console.WriteLine(element.Name + ", " + path + ", " + level);
        }

        public static void PrintFiles(FileInfo element, List<FileInfo> pathItems, int level)
        {
            string path = "";
            foreach (var item in pathItems)
            {
                path += "/" + item.Name;
            }
            Console.WriteLine(element.Name + ", " + path + ", " + level);
        }
    }
}
